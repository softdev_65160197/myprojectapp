package com.java.myapp;

import com.mongodb.MongoClient;
import java.util.List;

public class MongoDisplayDB {

    public static void main(String[] args) {
        try {
            MongoClient mongo = new MongoClient("localhost", 27017);
            List<String> dbs = mongo.getDatabaseNames();
            for (String db : dbs) {
                System.out.println(db);
            }
        } catch (Exception e) {
        }
    }
}
